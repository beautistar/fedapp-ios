Short description:

Player logs in, see's the main page with links to:

o	Menu link / Profile link

o	Room for CTA and banner ads

o	Ranking list link

o	Find club link

o	Social share/follow

o	Active/upcoming tournament

o	Live updates

o	Calendar

o	Newsfeed

Player can see lists of clubs, tournaments, players and news. There is a bit more to it, but this is the main purpose.

Super admin can log into admin website to edit/manage all info shown in the app.