//
//  Const.swift
//  FedApp
//
//  Created by Beautistar on 9/10/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation
import UIKit

struct Const {
    
    static let yellow1 = UIColor.init(red: 255, green: 197, blue: 0)
    static let red1 = UIColor.init(red: 238, green: 85, blue: 118)
    static let navBarColor = UIColor.init(red: 66, green: 69, blue: 79)
    static let profileBorderColor = UIColor.init(red: 85, green: 87, blue: 91)
    static let eventDateFontColor = UIColor.init(red: 0, green: 164, blue: 170)
    static let gray204 = UIColor.init(red: 204, green: 204, blue: 204)

}
