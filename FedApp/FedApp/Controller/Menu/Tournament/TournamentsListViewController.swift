//
//  TournamentsListViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/13/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class TournamentsListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        setNavigationBarItem()
        
        self.title = "TOURNAMENTS"
        let btnSetting = UIBarButtonItem(image: UIImage(named: "ic_search"), style: .plain, target: self, action: #selector(self.search)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = btnSetting
    }
    
    func search() {
        
    }
    
    // MARK: - Navigation
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 7
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (UIScreen.main.bounds.width - 20) * 570 / 1166 + 10 //10*2 ( width padding ), 1166:570 (tableview content view ratio), 10 (cell padding)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TournamentListLeftCell") as! TournamentListLeftCell
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TournamentListRightCell") as! TournamentListRightCell
            return cell
        }
    }
}
