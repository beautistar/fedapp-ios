//
//  TournamentDetailViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/15/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class TournamentDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    func initView() {
        
        setNavigationBarItem()
        
        self.title = "ABU DHABI FGB OPEN"
        
        let btnBack = UIBarButtonItem(image: UIImage(named: "ic_share"), style: .plain, target: self, action: #selector(self.shareAction))
        self.navigationItem.rightBarButtonItem  = btnBack
    }
    
    func shareAction() {
        
    }
}
