//
//  ProfileViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/14/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    weak var delegate: LeftMenuProtocol?

    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var tblInfoList: UITableView!
    
    @IBOutlet weak var viewTableContainter: UIView!
    @IBOutlet weak var constraintTableContainerHeight: NSLayoutConstraint!
    
    fileprivate let cellCount: Int = 14
    fileprivate let cellHeight: CGFloat = 25.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initView() {
        
        tblInfoList.layer.borderColor = Const.profileBorderColor.cgColor
        tblInfoList.layer.borderWidth = 1.0
        
        imvProfile.layer.borderColor = Const.profileBorderColor.cgColor
        
        setNavigationBarItem()
        
        self.title = "MORAD SENDESNI"
        
        let btnBack = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .plain, target: self, action: #selector(self.backAction))
        self.navigationItem.rightBarButtonItem  = btnBack
        
        
        /// match tableView Container
        let screenHeight = UIScreen.main.bounds.height
        var estimateHeight = screenHeight - viewTableContainter.frame.origin.y - 64 // 64 -> navigationbar height (64)
        
        let padding: CGFloat = 10.0
        let realHeight: CGFloat = cellHeight * CGFloat(cellCount) + padding * 2
        if realHeight < estimateHeight {
            estimateHeight = realHeight
        }
        self.constraintTableContainerHeight.constant = estimateHeight
        updateViewConstraints()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }

    
    func backAction() {
        
        delegate?.changeViewController(LeftMenu.Main)
        
    }
    
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cellCount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoListCell") as! ProfileInfoListCell
        return cell
    }

    
}
