//
//  RankingListViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/15/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class RankingListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    weak var delegate: LeftMenuProtocol?

    @IBOutlet weak var tblRankingList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        tblRankingList.tableFooterView = UIView()
        tblRankingList.layer.borderWidth = 1.0
        tblRankingList.layer.borderColor = Const.profileBorderColor.cgColor
        
        setNavigationBarItem()
        
        self.title = "RANKING LIST"
        let btnSetting = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .plain, target: self, action: #selector(self.backAction)) 
        self.navigationItem.rightBarButtonItem  = btnSetting
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: RankingListViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    
    func backAction() {
        
        delegate?.changeViewController(LeftMenu.Main)
    }

    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RankingListCell") as! RankingListCell
        
        if indexPath.row % 2 == 0 {
            
            cell.setWhiteLabel()
            
        } else {
            
            cell.setGrayLabel()
            
        }
        
        return cell
    }

}
