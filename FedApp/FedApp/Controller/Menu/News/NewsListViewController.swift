//
//  NewsListViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/13/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class NewsListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout {
    
    weak var delegate: LeftMenuProtocol?
    var w:CGFloat = 0
    
    var testString : [String] = ["aaaaaaaa", "bbbbbbbbbbbbbbbbbbbbbbbbb", "cccccccccccccccccccccccccccccccccccccccccccc"]
    
    
    
    @IBOutlet weak var cvNewsList: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        w = (self.view.frame.size.width - 20) / 2.0
        
        self.title = "TOURNAMENTS"
        let btnSetting = UIBarButtonItem(image: UIImage(named: "ic_search"), style: .plain, target: self, action: #selector(self.searchAction))
        self.navigationItem.rightBarButtonItem  = btnSetting
        
        let btnBack = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .plain, target: self, action: #selector(self.backAction))
        self.navigationItem.leftBarButtonItem  = btnBack
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 10.0
        layout.minimumInteritemSpacing = 10.0
        
        // Collection view attributes
        self.cvNewsList.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        self.cvNewsList.alwaysBounceVertical = true
        
        // Add the waterfall layout to your collection view
        self.cvNewsList.collectionViewLayout = layout
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: NewsListViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }

    
    func backAction() {
        
        delegate?.changeViewController(LeftMenu.Main)
    }
    
    func searchAction() {
        
    }
    

   
    // MARK: - CollectionView
    
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsListCell", for: indexPath) as! NewsListCell
        

        cell.lblTitle.text = testString[indexPath.row % 3]
        return cell
    }
    
    //MARK: - CollectionView Waterfall Layout Delegate Methods (Required)
    
    //** Size for the cells in the Waterfall Layout */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        // create a cell size from the image size, and return the size
   
        let strHeight = testString[indexPath.row % 3].height(withConstrainedWidth: w-20, font: UIFont.boldSystemFont(ofSize: 17))

        print("string height", strHeight)
        print("w", w)
        print("total cell height", 100+(w*43.0/57.0)+strHeight)
        
        return CGSize(width: CGFloat(w), height: 110+(w*43/57.0)+strHeight)
    }
    
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

//extension NewsListViewController : UICollectionViewDelegateFlowLayout {
//    

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        
//        return CGSize(width: CGFloat(w), height: CGFloat(w)
//        
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 5
//    }
//}
