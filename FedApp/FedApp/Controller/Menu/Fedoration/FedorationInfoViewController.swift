//
//  FedorationInfoViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/15/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class FedorationInfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func initView() {
        
        setNavigationBarItem()
        
        self.title = "UAE TENNIS FEDERATION"
        
        let btnBack = UIBarButtonItem(image: UIImage(named: "ic_share"), style: .plain, target: self, action: #selector(self.shareAction))
        self.navigationItem.rightBarButtonItem  = btnBack
    }
    
    func shareAction() {
        
    }

}
