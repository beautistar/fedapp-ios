//
//  MenuViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/11/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case Main = 0
    case Toumaments
    case RankingList
    case News
    case Clubs
    case FederationInfo
    case BecomeMember
    case Account
    case Setting
    case Logout
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class MenuViewController: UIViewController , LeftMenuProtocol, UITableViewDelegate, UITableViewDataSource {
    
    
    
    var menuNames = ["Toumaments", "Ranking list", "News", "Clubs", "Federation Info", "Become a member", "Your Account", "Settings", "Log Out"]
    
    var mainViewController: UIViewController!
    var toumamentsViewController: UIViewController!
    var rankingListViewController: UIViewController!
    var newsListViewController: UIViewController!
    var clubsViewController: UIViewController!
    var federationInfoViewController: UIViewController!
    var becomeMemberController: UIViewController!
    var yourAccountController: UIViewController!
    var settingController: UIViewController!
    
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var imvProfile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        lblTitle.font = UIFont(name: "Space Age", size: 23)
//        lblTitle.tintColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        /// Initialize MenuViewController
        tblMenu.tableFooterView = UIView()
        
       
        imvProfile.layer.borderColor = Const.profileBorderColor.cgColor
        
        /// initialize menu controllers
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let rankingListViewController = storyboard.instantiateViewController(withIdentifier: "RankingListViewController") as! RankingListViewController
        rankingListViewController.delegate = self
        self.rankingListViewController = UINavigationController(rootViewController: rankingListViewController)
        
        let tournamentListViewController = storyboard.instantiateViewController(withIdentifier: "TournamentsListViewController") as! TournamentsListViewController
        self.toumamentsViewController = UINavigationController(rootViewController: tournamentListViewController)
        
        let newsListViewController = storyboard.instantiateViewController(withIdentifier: "NewsListViewController") as! NewsListViewController
        newsListViewController.delegate = self
        self.newsListViewController = UINavigationController(rootViewController: newsListViewController)
        
        let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        profileViewController.delegate = self
        self.yourAccountController = UINavigationController(rootViewController: profileViewController)
        
        let clubListViewController = storyboard.instantiateViewController(withIdentifier: "ClubListViewController") as! ClubListViewController
        self.clubsViewController = UINavigationController(rootViewController: clubListViewController)
        
        let fedorationInfoViewController = storyboard.instantiateViewController(withIdentifier: "FedorationInfoViewController") as! FedorationInfoViewController
        self.federationInfoViewController = UINavigationController(rootViewController: fedorationInfoViewController)
        
        let becomeMemberViewController = storyboard.instantiateViewController(withIdentifier: "BecomeMemberViewController") as! BecomeMemberViewController
        becomeMemberViewController.delegate = self
        self.becomeMemberController = UINavigationController(rootViewController: becomeMemberViewController)
        
        let mainTabViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.mainViewController = UINavigationController(rootViewController: mainTabViewController)
        
    }
    
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
            
        case .Main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
            
        case .RankingList:
            self.slideMenuController()?.changeMainViewController(self.rankingListViewController, close: true)
         
        case .Toumaments:
            self.slideMenuController()?.changeMainViewController(self.toumamentsViewController, close: true)
        case .News:
            self.slideMenuController()?.changeMainViewController(self.newsListViewController, close: true)
            
        case .Clubs:
            self.slideMenuController()?.changeMainViewController(self.clubsViewController, close: true)
            
        case .Account:
            self.slideMenuController()?.changeMainViewController(self.yourAccountController, close: true)
        
        case .FederationInfo:
            self.slideMenuController()?.changeMainViewController(self.federationInfoViewController, close: true)
            
        case .BecomeMember:
            self.slideMenuController()?.changeMainViewController(self.becomeMemberController, close: true)
            

        case .Logout:
            self.logout()
            
        default : 
            
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
            
        }
    }
    
    func logout() {
    
        let loginNav = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        
        UIApplication.shared.keyWindow?.rootViewController = loginNav
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
    }
    
    // MARK: - Navigation
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuNames.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row+1) {
            self.changeViewController(menu)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell")!
        
        cell.textLabel?.text = menuNames[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        
        return cell
    }

}
