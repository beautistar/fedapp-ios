//
//  MainViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/11/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    let btnTextAttribute : [String: Any] = [
        NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14),
        NSForegroundColorAttributeName : Const.eventDateFontColor,
        NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue]

    var btnProfile:UIBarButtonItem!
    
    @IBOutlet weak var lblLiveUpdateDate: UILabel!
    @IBOutlet weak var lblEventDate1: UILabel!
    @IBOutlet weak var lblEventDate2: UILabel!
    @IBOutlet weak var lblEventDate3: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initView() {
        
        let liveupdateDateStr = NSMutableAttributedString(string: "9 Oct - 12 Oct 2016", attributes: btnTextAttribute)
        lblLiveUpdateDate.attributedText = liveupdateDateStr
        lblEventDate1.attributedText = liveupdateDateStr
        lblEventDate2.attributedText = liveupdateDateStr
        lblEventDate3.attributedText = liveupdateDateStr       
        
        
        setNavigationBarItem()
        self.navigationItem.title = "WELCOME TO FEDAPP"
        btnProfile = UIBarButtonItem(image: UIImage(named: "ic_profile"), style:.plain,target: self, action: #selector(MainViewController.gotoProfile))
        btnProfile.image = btnProfile.image?.withRenderingMode(.alwaysOriginal)
        
        self.navigationItem.rightBarButtonItem  = btnProfile
        
    }
    
    func gotoProfile() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
