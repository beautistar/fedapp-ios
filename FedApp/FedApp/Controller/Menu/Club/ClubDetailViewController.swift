//
//  ClubDetailViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/15/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class ClubDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        setNavigationBarItem()
        
        self.title = "MORAD SENDESNI"
        
        let btnBack = UIBarButtonItem(image: UIImage(named: "ic_share"), style: .plain, target: self, action: #selector(self.shareAction))
        self.navigationItem.rightBarButtonItem  = btnBack
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.popViewController(animated: false)
    }
    
    func shareAction() {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
