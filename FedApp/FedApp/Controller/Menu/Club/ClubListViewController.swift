//
//  ClubListViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/14/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class ClubListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {   
    

    @IBOutlet weak var tblClubList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        tblClubList.tableFooterView = UIView()
        
        setNavigationBarItem()
        
        self.title = "MORAD SENDESNI"
        
        let btnBack = UIBarButtonItem(image: UIImage(named: "ic_share"), style: .plain, target: self, action: #selector(self.shareAction))
        self.navigationItem.rightBarButtonItem  = btnBack
    }

    func shareAction() {
        
    }
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row % 4 == 0 {
            return 30
        } else {
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 4 == 0 {
            
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "ClubListHeaderCell") as! ClubListHeaderCell
            return headerCell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClubListCell") as! ClubListCell
            return cell
        }
    }
}
