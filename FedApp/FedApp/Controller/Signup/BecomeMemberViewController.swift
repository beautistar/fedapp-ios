//
//  BecomeMemberViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/10/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class BecomeMemberViewController: BaseViewController, UITextFieldDelegate {
    
     weak var delegate: LeftMenuProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: NewsListViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        
        delegate?.changeViewController(LeftMenu.Main)
    }
    
    
    //MARK:- TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
}
