//
//  BecomeMember2ViewController.swift
//  FedApp
//
//  Created by Beautistar on 9/10/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class BecomeMember2ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func backACtion(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)        
    }
}
