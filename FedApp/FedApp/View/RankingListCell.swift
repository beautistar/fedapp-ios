//
//  RankingListCell.swift
//  FedApp
//
//  Created by Beautistar on 9/15/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class RankingListCell: UITableViewCell {
    
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblClub: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setWhiteLabel() {
        
        lblRank.backgroundColor = UIColor.white
        lblName.backgroundColor = UIColor.white
        lblAge.backgroundColor = UIColor.white
        lblClub.backgroundColor = UIColor.white
    }
    
    func setGrayLabel() {
        
        lblRank.backgroundColor = Const.gray204
        lblName.backgroundColor = Const.gray204
        lblAge.backgroundColor = Const.gray204
        lblClub.backgroundColor = Const.gray204
        
    }

}
