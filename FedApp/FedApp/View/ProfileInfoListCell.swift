//
//  ProfileInfoListCell.swift
//  FedApp
//
//  Created by Beautistar on 9/14/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class ProfileInfoListCell: UITableViewCell {
    
    @IBOutlet weak var lblFirstKey: UILabel!
    @IBOutlet weak var lblFirstValue: UILabel!
    @IBOutlet weak var lblSecondKey: UILabel!
    @IBOutlet weak var lblSecondValue: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
